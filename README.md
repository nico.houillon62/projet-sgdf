# projet-sgdf

Site web permettant de faciliter la gestion et la création d'un dossier de camp scout (gestion de matériel, gestion des activités, effectifs, etc...)

### À faire

- pouvoir ouvrir un dossier de camp
- créer des rôles (chef bleu/lvt/pio, jeune, maitrise, admin)
- Avoir une liste de l'effectif en fonction de son rôle
- Avoir une liste du matériel 
- pouvoir ajouter supprimer du matériel (créer un rôle spécial ?)
- pouvoir réserver du matériel
- Faire une liste de menus 
- pouvoir entrer un lieu de camp dans un dossier de camp
- faire une liste des lieux déjà "utilisés"
- pouvoir inscrire les jeunes/chefs de sa branche pour un camp
- pouvoir importer des fichiers (fiches activités/budget)
- Faire un formulaire pour les analyses d'unité
- pouvoir exporter l'analyse d'unité en pdf
- pouvoir faire un planning d'activités
- pouvoir faire un planning de menus
- pouvoir faire une journée type
- Faire une boîte à idées

