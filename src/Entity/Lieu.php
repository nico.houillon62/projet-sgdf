<?php

namespace App\Entity;

use App\Repository\LieuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LieuRepository::class)
 */
class Lieu
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="integer")
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ville;

    /**
     * @ORM\ManyToMany(targetEntity=Intervenant::class, inversedBy="lieus")
     */
    private $proprietaire;

    /**
     * @ORM\OneToMany(targetEntity=LieuCamp::class, mappedBy="lieu", orphanRemoval=true)
     */
    private $lieuCamps;

    public function __construct()
    {
        $this->proprietaire = new ArrayCollection();
        $this->lieuCamps = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodePostal(): ?int
    {
        return $this->codePostal;
    }

    public function setCodePostal(int $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * @return Collection|Intervenant[]
     */
    public function getProprietaire(): Collection
    {
        return $this->proprietaire;
    }

    public function addProprietaire(Intervenant $proprietaire): self
    {
        if (!$this->proprietaire->contains($proprietaire)) {
            $this->proprietaire[] = $proprietaire;
        }

        return $this;
    }

    public function removeProprietaire(Intervenant $proprietaire): self
    {
        $this->proprietaire->removeElement($proprietaire);

        return $this;
    }

    /**
     * @return Collection|LieuCamp[]
     */
    public function getLieuCamps(): Collection
    {
        return $this->lieuCamps;
    }

    public function addLieuCamp(LieuCamp $lieuCamp): self
    {
        if (!$this->lieuCamps->contains($lieuCamp)) {
            $this->lieuCamps[] = $lieuCamp;
            $lieuCamp->setLieu($this);
        }

        return $this;
    }

    public function removeLieuCamp(LieuCamp $lieuCamp): self
    {
        if ($this->lieuCamps->removeElement($lieuCamp)) {
            // set the owning side to null (unless already changed)
            if ($lieuCamp->getLieu() === $this) {
                $lieuCamp->setLieu(null);
            }
        }

        return $this;
    }
}
