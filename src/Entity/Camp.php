<?php

namespace App\Entity;

use App\Repository\CampRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CampRepository::class)
 */
class Camp
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="datetime")
     */
    private $debut;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fin;

    /**
     * @ORM\ManyToOne(targetEntity=Branche::class, inversedBy="camps")
     * @ORM\JoinColumn(nullable=false)
     */
    private $branche;

    /**
     * @ORM\OneToMany(targetEntity=Participant::class, mappedBy="camp", orphanRemoval=true)
     */
    private $participants;

    /**
     * @ORM\OneToMany(targetEntity=Location::class, mappedBy="camp", orphanRemoval=true)
     */
    private $locations;

    /**
     * @ORM\ManyToMany(targetEntity=Activity::class, mappedBy="camp")
     */
    private $activities;

    /**
     * @ORM\OneToMany(targetEntity=LieuCamp::class, mappedBy="camp", orphanRemoval=true)
     */
    private $lieuCamps;

    /**
     * @ORM\OneToMany(targetEntity=Menu::class, mappedBy="camp")
     */
    private $menus;

    public function __construct()
    {
        $this->participants = new ArrayCollection();
        $this->locations = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->lieuCamps = new ArrayCollection();
        $this->menus = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDebut(): ?\DateTimeInterface
    {
        return $this->debut;
    }

    public function setDebut(\DateTimeInterface $debut): self
    {
        $this->debut = $debut;

        return $this;
    }

    public function getFin(): ?\DateTimeInterface
    {
        return $this->fin;
    }

    public function setFin(\DateTimeInterface $fin): self
    {
        $this->fin = $fin;

        return $this;
    }

    public function getBranche(): ?Branche
    {
        return $this->branche;
    }

    public function setBranche(?Branche $branche): self
    {
        $this->branche = $branche;

        return $this;
    }

    /**
     * @return Collection|Participant[]
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function addParticipant(Participant $participant): self
    {
        if (!$this->participants->contains($participant)) {
            $this->participants[] = $participant;
            $participant->setCamp($this);
        }

        return $this;
    }

    public function removeParticipant(Participant $participant): self
    {
        if ($this->participants->removeElement($participant)) {
            // set the owning side to null (unless already changed)
            if ($participant->getCamp() === $this) {
                $participant->setCamp(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Location[]
     */
    public function getLocations(): Collection
    {
        return $this->locations;
    }

    public function addLocation(Location $location): self
    {
        if (!$this->locations->contains($location)) {
            $this->locations[] = $location;
            $location->setCamp($this);
        }

        return $this;
    }

    public function removeLocation(Location $location): self
    {
        if ($this->locations->removeElement($location)) {
            // set the owning side to null (unless already changed)
            if ($location->getCamp() === $this) {
                $location->setCamp(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Activity[]
     */
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    public function addActivity(Activity $activity): self
    {
        if (!$this->activities->contains($activity)) {
            $this->activities[] = $activity;
            $activity->addCamp($this);
        }

        return $this;
    }

    public function removeActivity(Activity $activity): self
    {
        if ($this->activities->removeElement($activity)) {
            $activity->removeCamp($this);
        }

        return $this;
    }

    /**
     * @return Collection|LieuCamp[]
     */
    public function getLieuCamps(): Collection
    {
        return $this->lieuCamps;
    }

    public function addLieuCamp(LieuCamp $lieuCamp): self
    {
        if (!$this->lieuCamps->contains($lieuCamp)) {
            $this->lieuCamps[] = $lieuCamp;
            $lieuCamp->setCamp($this);
        }

        return $this;
    }

    public function removeLieuCamp(LieuCamp $lieuCamp): self
    {
        if ($this->lieuCamps->removeElement($lieuCamp)) {
            // set the owning side to null (unless already changed)
            if ($lieuCamp->getCamp() === $this) {
                $lieuCamp->setCamp(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getMenus(): Collection
    {
        return $this->menus;
    }

    public function addMenu(Menu $menu): self
    {
        if (!$this->menus->contains($menu)) {
            $this->menus[] = $menu;
            $menu->setCamp($this);
        }

        return $this;
    }

    public function removeMenu(Menu $menu): self
    {
        if ($this->menus->removeElement($menu)) {
            // set the owning side to null (unless already changed)
            if ($menu->getCamp() === $this) {
                $menu->setCamp(null);
            }
        }

        return $this;
    }
}
