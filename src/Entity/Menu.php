<?php

namespace App\Entity;

use App\Repository\MenuRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MenuRepository::class)
 */
class Menu
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Camp::class, inversedBy="menus")
     * @ORM\JoinColumn(nullable=false)
     */
    private $camp;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=Plat::class)
     */
    private $petitDej;

    /**
     * @ORM\ManyToOne(targetEntity=Plat::class)
     */
    private $dejeuner;

    /**
     * @ORM\ManyToOne(targetEntity=Plat::class)
     */
    private $gouter;

    /**
     * @ORM\ManyToOne(targetEntity=Plat::class)
     */
    private $diner;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCamp(): ?Camp
    {
        return $this->camp;
    }

    public function setCamp(?Camp $camp): self
    {
        $this->camp = $camp;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPetitDej(): ?Plat
    {
        return $this->petitDej;
    }

    public function setPetitDej(?Plat $petitDej): self
    {
        $this->petitDej = $petitDej;

        return $this;
    }

    public function getDejeuner(): ?Plat
    {
        return $this->dejeuner;
    }

    public function setDejeuner(?Plat $dejeuner): self
    {
        $this->dejeuner = $dejeuner;

        return $this;
    }

    public function getGouter(): ?Plat
    {
        return $this->gouter;
    }

    public function setGouter(?Plat $gouter): self
    {
        $this->gouter = $gouter;

        return $this;
    }

    public function getDiner(): ?Plat
    {
        return $this->diner;
    }

    public function setDiner(?Plat $diner): self
    {
        $this->diner = $diner;

        return $this;
    }
}
