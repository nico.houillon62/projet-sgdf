<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210821183744 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE activity (id INT AUTO_INCREMENT NOT NULL, responsable_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, date DATE DEFAULT NULL, rules LONGTEXT DEFAULT NULL, nb_joueurs INT DEFAULT NULL, duree TIME DEFAULT NULL, materiel LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', image VARCHAR(255) DEFAULT NULL, INDEX IDX_AC74095A53C59D72 (responsable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE activity_camp (activity_id INT NOT NULL, camp_id INT NOT NULL, INDEX IDX_C25099A281C06096 (activity_id), INDEX IDX_C25099A277075ABB (camp_id), PRIMARY KEY(activity_id, camp_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE branche (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, couleur VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE camp (id INT AUTO_INCREMENT NOT NULL, branche_id INT NOT NULL, nom VARCHAR(255) NOT NULL, debut DATETIME NOT NULL, fin DATETIME NOT NULL, INDEX IDX_C19442309DDF9A9E (branche_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE intervenant (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) DEFAULT NULL, titre VARCHAR(255) NOT NULL, telephone VARCHAR(255) DEFAULT NULL, mail VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lieu (id INT AUTO_INCREMENT NOT NULL, adresse VARCHAR(255) NOT NULL, code_postal INT NOT NULL, ville VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lieu_intervenant (lieu_id INT NOT NULL, intervenant_id INT NOT NULL, INDEX IDX_BF8909886AB213CC (lieu_id), INDEX IDX_BF890988AB9A1716 (intervenant_id), PRIMARY KEY(lieu_id, intervenant_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lieu_camp (id INT AUTO_INCREMENT NOT NULL, camp_id INT NOT NULL, lieu_id INT NOT NULL, debut DATE NOT NULL, fin DATE NOT NULL, autorisation VARCHAR(255) DEFAULT NULL, plan VARCHAR(255) DEFAULT NULL, INDEX IDX_4B08E78877075ABB (camp_id), INDEX IDX_4B08E7886AB213CC (lieu_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location (id INT AUTO_INCREMENT NOT NULL, materiel_id INT NOT NULL, camp_id INT NOT NULL, debut DATE NOT NULL, fin DATE NOT NULL, INDEX IDX_5E9E89CB16880AAF (materiel_id), INDEX IDX_5E9E89CB77075ABB (camp_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE materiel (id INT AUTO_INCREMENT NOT NULL, categorie INT NOT NULL, nom VARCHAR(255) NOT NULL, quantit� INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu (id INT AUTO_INCREMENT NOT NULL, camp_id INT NOT NULL, petit_dej_id INT DEFAULT NULL, dejeuner_id INT DEFAULT NULL, gouter_id INT DEFAULT NULL, diner_id INT DEFAULT NULL, date DATE NOT NULL, INDEX IDX_7D053A9377075ABB (camp_id), INDEX IDX_7D053A9383D5D7FC (petit_dej_id), INDEX IDX_7D053A9368482329 (dejeuner_id), INDEX IDX_7D053A93F0C77687 (gouter_id), INDEX IDX_7D053A9314B79A3C (diner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE participant (id INT AUTO_INCREMENT NOT NULL, users_id INT NOT NULL, camp_id INT NOT NULL, statut INT NOT NULL, role INT DEFAULT NULL, debut DATE NOT NULL, fin DATE NOT NULL, INDEX IDX_D79F6B1167B3B43D (users_id), INDEX IDX_D79F6B1177075ABB (camp_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE plat (id INT AUTO_INCREMENT NOT NULL, entree VARCHAR(255) DEFAULT NULL, plat VARCHAR(255) NOT NULL, dessert VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, branche_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), INDEX IDX_8D93D6499DDF9A9E (branche_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE activity ADD CONSTRAINT FK_AC74095A53C59D72 FOREIGN KEY (responsable_id) REFERENCES participant (id)');
        $this->addSql('ALTER TABLE activity_camp ADD CONSTRAINT FK_C25099A281C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE activity_camp ADD CONSTRAINT FK_C25099A277075ABB FOREIGN KEY (camp_id) REFERENCES camp (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE camp ADD CONSTRAINT FK_C19442309DDF9A9E FOREIGN KEY (branche_id) REFERENCES branche (id)');
        $this->addSql('ALTER TABLE lieu_intervenant ADD CONSTRAINT FK_BF8909886AB213CC FOREIGN KEY (lieu_id) REFERENCES lieu (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE lieu_intervenant ADD CONSTRAINT FK_BF890988AB9A1716 FOREIGN KEY (intervenant_id) REFERENCES intervenant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE lieu_camp ADD CONSTRAINT FK_4B08E78877075ABB FOREIGN KEY (camp_id) REFERENCES camp (id)');
        $this->addSql('ALTER TABLE lieu_camp ADD CONSTRAINT FK_4B08E7886AB213CC FOREIGN KEY (lieu_id) REFERENCES lieu (id)');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CB16880AAF FOREIGN KEY (materiel_id) REFERENCES materiel (id)');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CB77075ABB FOREIGN KEY (camp_id) REFERENCES camp (id)');
        $this->addSql('ALTER TABLE menu ADD CONSTRAINT FK_7D053A9377075ABB FOREIGN KEY (camp_id) REFERENCES camp (id)');
        $this->addSql('ALTER TABLE menu ADD CONSTRAINT FK_7D053A9383D5D7FC FOREIGN KEY (petit_dej_id) REFERENCES plat (id)');
        $this->addSql('ALTER TABLE menu ADD CONSTRAINT FK_7D053A9368482329 FOREIGN KEY (dejeuner_id) REFERENCES plat (id)');
        $this->addSql('ALTER TABLE menu ADD CONSTRAINT FK_7D053A93F0C77687 FOREIGN KEY (gouter_id) REFERENCES plat (id)');
        $this->addSql('ALTER TABLE menu ADD CONSTRAINT FK_7D053A9314B79A3C FOREIGN KEY (diner_id) REFERENCES plat (id)');
        $this->addSql('ALTER TABLE participant ADD CONSTRAINT FK_D79F6B1167B3B43D FOREIGN KEY (users_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE participant ADD CONSTRAINT FK_D79F6B1177075ABB FOREIGN KEY (camp_id) REFERENCES camp (id)');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D6499DDF9A9E FOREIGN KEY (branche_id) REFERENCES branche (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE activity_camp DROP FOREIGN KEY FK_C25099A281C06096');
        $this->addSql('ALTER TABLE camp DROP FOREIGN KEY FK_C19442309DDF9A9E');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D6499DDF9A9E');
        $this->addSql('ALTER TABLE activity_camp DROP FOREIGN KEY FK_C25099A277075ABB');
        $this->addSql('ALTER TABLE lieu_camp DROP FOREIGN KEY FK_4B08E78877075ABB');
        $this->addSql('ALTER TABLE location DROP FOREIGN KEY FK_5E9E89CB77075ABB');
        $this->addSql('ALTER TABLE menu DROP FOREIGN KEY FK_7D053A9377075ABB');
        $this->addSql('ALTER TABLE participant DROP FOREIGN KEY FK_D79F6B1177075ABB');
        $this->addSql('ALTER TABLE lieu_intervenant DROP FOREIGN KEY FK_BF890988AB9A1716');
        $this->addSql('ALTER TABLE lieu_intervenant DROP FOREIGN KEY FK_BF8909886AB213CC');
        $this->addSql('ALTER TABLE lieu_camp DROP FOREIGN KEY FK_4B08E7886AB213CC');
        $this->addSql('ALTER TABLE location DROP FOREIGN KEY FK_5E9E89CB16880AAF');
        $this->addSql('ALTER TABLE activity DROP FOREIGN KEY FK_AC74095A53C59D72');
        $this->addSql('ALTER TABLE menu DROP FOREIGN KEY FK_7D053A9383D5D7FC');
        $this->addSql('ALTER TABLE menu DROP FOREIGN KEY FK_7D053A9368482329');
        $this->addSql('ALTER TABLE menu DROP FOREIGN KEY FK_7D053A93F0C77687');
        $this->addSql('ALTER TABLE menu DROP FOREIGN KEY FK_7D053A9314B79A3C');
        $this->addSql('ALTER TABLE participant DROP FOREIGN KEY FK_D79F6B1167B3B43D');
        $this->addSql('DROP TABLE activity');
        $this->addSql('DROP TABLE activity_camp');
        $this->addSql('DROP TABLE branche');
        $this->addSql('DROP TABLE camp');
        $this->addSql('DROP TABLE intervenant');
        $this->addSql('DROP TABLE lieu');
        $this->addSql('DROP TABLE lieu_intervenant');
        $this->addSql('DROP TABLE lieu_camp');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP TABLE materiel');
        $this->addSql('DROP TABLE menu');
        $this->addSql('DROP TABLE participant');
        $this->addSql('DROP TABLE plat');
        $this->addSql('DROP TABLE `user`');
    }
}
